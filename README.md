README студента Яндекс.Практикума 
-
Краткое описание проектов:
|Название и статус|Описание проекта|Стек технологий|Ссылка на проект|
|--|--|--|--|
|AB_project, завершён |Необходимо на основании приведённых данных дать ответ о наличии различий в группах А и В проводившегося теста, провести приоритезацию гипотез|pandas, seaborn, numpy, math, stats from scipy|https://gitlab.com/igorknazz/my_practicum_projects/-/blob/main/AB_project/AB_project.ipynb|
|DEBT_project, завершён|Предполагаемый заказчик исследования - кредитный отдел некоего банка. Необходимо провести предварительную очистку датасета, провести анализ заёмщиков и в итоге выдать маску клиента, который с наибольшей вероятностью не будет задерживать выплаты|pandas, numpy, seaborn, counter, Mystem|https://gitlab.com/igorknazz/my_practicum_projects/-/blob/main/Debt_project/Debt_project.ipynb|
|MKRF_project, завершён|Цель проекта - проанализировать фильмы, по которым есть информация в госданных. Особый акцент в исследовании будет стоять на фильмах с господдержкой|pandas, numpy, pyplot, Mystem|https://gitlab.com/igorknazz/my_practicum_projects/-/blob/main/MKRF_project/MKRF_project.ipynb|
|REST_project, завершён|Цель исследование - выяснить, есть ли у проекта инвесторов долгие перспективы|pandas, numpy, seaborn, plotly, pyplot|https://gitlab.com/igorknazz/my_practicum_projects/-/blob/main/Rest_project/Rest_project.ipynb|
|Tariffs_project, завершён| Задача - проанализировать поведение и профили клиентов за 2018 год, на основании полученных данных сделать выводы о большей прибыльности одного из двух исследуемых тарифов|pandas, numpy, seaborn, stats from scipy, math|https://gitlab.com/igorknazz/my_practicum_projects/-/blob/main/Triffs_project/Tariffs_project.ipynb|
|UNIT_ANALYTICS_project, завершён|Предстоит изучить, как люди пользуются продуктом, когда они начинают покупать, сколько денег приносит каждый клиент, когда он окупается и какие факторы отрицательно влияют на привлечение пользователей.|pandas, numpy, seaborn, pyplot|https://gitlab.com/igorknazz/my_practicum_projects/-/blob/main/Unit_analytics_project/Unit_analitics.ipynb|

Краткая информация обо мне:

В период с августа 2021 по сентябрь 2022 учился в Яндекс.Практикуме на специальность Аналитика данных, курс закончил. В данный момент учусь в Финансовом Университете на факультете прикладной информатики на превом курсе
Знаком с технологиями Git, ООП. Пишу в основном на Python, в некоторой степени знаю GO и Ruby, умею работать с BI-системами (Tableau, PowerBI), знаком с асинхронностью. Увлекаюсь математической статистикой. Работе и учёбе готов посвящать всё свободное время, учиться люблю. Большой поклонник порядка во всём, особенно что касается работы.
